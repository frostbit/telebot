package tk.frostbit.telegram.echobot;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tk.frostbit.telebot.Bot;
import tk.frostbit.telebot.CommandHandler;
import tk.frostbit.telebot.api.SendTextMessage;
import tk.frostbit.telebot.model.CallbackQuery;
import tk.frostbit.telebot.model.InlineKeyboardMarkup;
import tk.frostbit.telebot.model.Message;

public class EchoCommandHandler extends CommandHandler {

    public EchoCommandHandler(Bot bot) {
        super(bot);
    }

    @Override
    protected boolean handleCommandMessage(@NotNull String command, @Nullable String query, @NotNull Message message) {
        System.out.println("Incoming command: " + command + " [" + query + "]");

        reply(message, "Command: " + command + "\nQuery: " + query);
        return true;
    }

    @Override
    protected void handleArbitraryMessage(Message message) {
        System.out.println("Incoming message: " + message);

        final String requestText = message.getText();
        if (requestText == null) {
            reply(message, "Please send me text");
        } else {
            reply(message, "Echo: " + requestText);
        }
    }

    @Override
    protected void handleCallbackQuery(CallbackQuery query) {
        System.out.println("Callback query: " + query);

        reply(query.getMessage(), "Callback: " + query.getData());
    }

    private void reply(Message incoming, String text) {
        final SendTextMessage request = new SendTextMessage(incoming, text);
        request.setReplyMarkup(new InlineKeyboardMarkup.Builder()
                .addCallback("Click me!", "top-left")
                .addUrl("Click me!", "https://github.com/fRoSt13iT/telebot")
                .nextRow()
                .addCallback("Click me!", "bottom")
                .build());
        makeApiRequest(request);
    }

}
