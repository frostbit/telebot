package tk.frostbit.telegram.echobot;

import org.jetbrains.annotations.NotNull;
import tk.frostbit.telebot.Bot;
import tk.frostbit.telebot.BotConfig;
import tk.frostbit.telebot.CommandHandler;

public class EchoBot extends Bot {

    public EchoBot(BotConfig config) {
        super(config);
    }

    @NotNull
    @Override
    protected CommandHandler createCommandHandler() {
        return new EchoCommandHandler(this);
    }
}
