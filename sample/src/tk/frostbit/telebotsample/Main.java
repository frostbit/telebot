package tk.frostbit.telebotsample;

import tk.frostbit.telebot.BotConfig;
import tk.frostbit.telebot.SimpleBotConfig;
import tk.frostbit.telebot.exceptions.ApiException;
import tk.frostbit.telegram.echobot.EchoBot;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class Main {

    private static final String TOKEN = "token";

    public static void main(String[] args) throws ApiException {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "debug");

        final Properties properties = new Properties();
        try (InputStream is = new FileInputStream("local.properties")) {
            properties.load(is);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        final BotConfig config = new SimpleBotConfig()
                .setToken(properties.getProperty(TOKEN));
        final EchoBot echoBot = new EchoBot(config);
        echoBot.start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Shutting down");
            try {
                echoBot.shutdown();
            } catch (InterruptedException ignored) { }
        }));
    }
}
