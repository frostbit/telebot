package tk.frostbit.telebot.api;

import org.json.JSONObject;
import tk.frostbit.telebot.model.InlineKeyboardMarkup;
import tk.frostbit.telebot.model.Message;
import tk.frostbit.telebot.model.ParseMode;

public class EditMessageText extends Request<Message> {
    private EditMessageText(String newText) {
        super("editMessageText");
        putParameter("text", newText);
    }

    public EditMessageText(long chatId, long messageId, String newText) {
        this(newText);
        putParameter("chat_id", chatId);
        putParameter("message_id", messageId);
    }

    public EditMessageText(Message message, String newText) {
        this(message.getChat().getId(), message.getId(), newText);
    }

    // TODO: support for inline messages (ctor with inline_message_id)

    public EditMessageText setParseMode(ParseMode mode) {
        putParameter("parse_mode", mode.getApiValue());
        return this;
    }

    public EditMessageText setDisablePreview(boolean value) {
        putParameter("disable_web_page_preview", value);
        return this;
    }

    public EditMessageText setReplyMarkup(InlineKeyboardMarkup inlineKeyboard) {
        putParameter("reply_markup", inlineKeyboard.toJSONString());
        return this;
    }

    @Override
    public Message parseResult(Object result) {
        return new Message((JSONObject) result);
    }
}
