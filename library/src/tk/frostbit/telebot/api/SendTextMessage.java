package tk.frostbit.telebot.api;

import org.json.JSONObject;
import tk.frostbit.telebot.model.InlineKeyboardMarkup;
import tk.frostbit.telebot.model.Message;
import tk.frostbit.telebot.model.ParseMode;

/**
 * Invokes {@code sendMessage} API method.
 *
 * <p>
 *     Use this method to send text messages. On success, the sent Message is returned.
 *     <br/><i>(from Telegram API documentation)</i>
 * </p>
 */
public class SendTextMessage extends Request<Message> {

    public SendTextMessage(long chatId, String text) {
        super("sendMessage");
        putParameter("chat_id", chatId);
        putParameter("text", text);
    }

    public SendTextMessage(Message replyTo, String text) {
        this(replyTo.getChat().getId(), text);
        setReplyTo(replyTo);
    }

    public SendTextMessage setParseMode(ParseMode mode) {
        putParameter("parse_mode", mode.getApiValue());
        return this;
    }

    public SendTextMessage setDisablePreview(boolean value) {
        putParameter("disable_web_page_preview", value);
        return this;
    }

    public SendTextMessage setDisableNotification(boolean value) {
        putParameter("disable_notification", value);
        return this;
    }

    public SendTextMessage setReplyTo(Message originalMessage) {
        return setReplyTo(originalMessage.getId());
    }

    public SendTextMessage setReplyTo(long originalMessageId) {
        putParameter("reply_to_message_id", originalMessageId);
        return this;
    }

    public SendTextMessage setReplyMarkup(InlineKeyboardMarkup inlineKeyboard) {
        putParameter("reply_markup", inlineKeyboard.toJSONString());
        return this;
    }

    @Override
    public Message parseResult(Object result) {
        return new Message((JSONObject) result);
    }
}
