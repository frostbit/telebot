package tk.frostbit.telebot.api;

import org.json.JSONObject;

import java.util.*;

/**
 * Request to Telegram API.
 *
 * @param <R> type of result
 */
public abstract class Request<R> {

    private final String method;
    private final Map<String, Object> parameters;

    protected Request(String method) {
        this.method = method;
        this.parameters = new HashMap<>();
    }

    public final String getMethod() {
        return method;
    }

    public final Map<String, Object> getParameters() {
        return Collections.unmodifiableMap(parameters);
    }

    protected final void putParameter(String name, Object value) {
        parameters.put(name, value);
    }

    public abstract R parseResult(Object result);

}
