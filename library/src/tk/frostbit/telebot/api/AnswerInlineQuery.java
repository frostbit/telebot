package tk.frostbit.telebot.api;

import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class AnswerInlineQuery extends Request<Boolean> {
    public AnswerInlineQuery(String queryId, String messageText) {
        super("answerInlineQuery");
        putParameter("inline_query_id", queryId);

        final JSONArray results = new JSONArray();
        final JSONObject result = new JSONObject();
        result.put("type", "article");
        result.put("id", DigestUtils.md5Hex(messageText));
        result.put("title", messageText);
        final JSONObject messageContent = new JSONObject();
        messageContent.put("message_text", messageText);
        result.put("input_message_content", messageContent);
        results.put(result);
        putParameter("results", results);
    }

    @Override
    public Boolean parseResult(Object result) {
        return result.equals(true);
    }
}
