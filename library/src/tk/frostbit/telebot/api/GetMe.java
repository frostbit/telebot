package tk.frostbit.telebot.api;

import org.json.JSONObject;
import tk.frostbit.telebot.model.User;

/**
 * A simple method for testing your bot's auth token.
 * Requires no parameters.
 * Returns basic information about the bot in form of a User object.
 * <br/><i>(from Telegram API documentation)</i>
 */
public class GetMe extends Request<User> {
    public GetMe() {
        super("getMe");
    }

    @Override
    public User parseResult(Object result) {
        return new User((JSONObject) result);
    }
}
