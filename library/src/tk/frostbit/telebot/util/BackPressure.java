package tk.frostbit.telebot.util;

import java.util.*;

/**
 * Helps to implement <a href="http://www.reactivemanifesto.org/glossary#Back-Pressure">back-pressure</a>.
 * <br/> This class is thread-safe.
 */
public class BackPressure {

    private final Set<Producer> producers = new HashSet<>();
    private final Set<ConsumerHandle> consumers = new HashSet<>();
    private final Set<ConsumerHandle> overloadedConsumers = new HashSet<>();
    private final Object mutex = new Object();

    public void registerProducer(Producer producer) {
        synchronized (mutex) {
            if (!producers.contains(producer)) {
                producers.add(producer);
            }
            if (!overloadedConsumers.isEmpty()) {
                producer.pause();
            }
        }
    }

    public void unregisterProducer(Producer producer) {
        synchronized (mutex) {
            producers.remove(producer);
        }
    }

    public ConsumerHandle newConsumer() {
        synchronized (mutex) {
            final ConsumerHandle consumer = new ConsumerHandle();
            consumers.add(consumer);
            return consumer;
        }
    }

    public void unregisterConsumer(ConsumerHandle consumer) {
        synchronized (mutex) {
            if (consumers.contains(consumer)) {
                notifyConsumerOkay(consumer);
                consumers.remove(consumer);
            }
        }
    }

    private void notifyConsumerOverloaded(ConsumerHandle consumer) {
        synchronized (mutex) {
            if (consumers.contains(consumer)) {
                final boolean added = overloadedConsumers.add(consumer);
                if (added && overloadedConsumers.size() == 1) {
                    producers.forEach(Producer::pause);
                }
            }
        }
    }

    private void notifyConsumerOkay(ConsumerHandle consumer) {
        synchronized (mutex) {
            if (consumers.contains(consumer)) {
                final boolean removed = overloadedConsumers.remove(consumer);
                if (removed &&  overloadedConsumers.size() == 0) {
                    producers.forEach(Producer::resume);
                }
            }
        }
    }

    public interface Producer {

        /**
         * Called when one of the consumers is overloaded.
         *
         * <p> <b>Notice:</b> this method may be called from any thread
         * and should not register or unregister consumers and producers
         * in associated {@link BackPressure} instance. </p>
         */
        void pause();

        /**
         * Called when all of the consumers are okay.
         *
         * <p> <b>Notice:</b> this method may be called from any thread
         * and should not register or unregister consumers and producers
         * in associated {@link BackPressure} instance. </p>
         */
        void resume();

    }

    public class ConsumerHandle {

        /**
         * Notify back-pressure implementation that this consumer is overloaded.
         * All producers will be requested to pause.
         */
        public void notifyOverloaded() {
            BackPressure.this.notifyConsumerOverloaded(this);
        }

        /**
         * Notify back-pressure implementation that this consumer is left overloaded state.
         * If all other consumers are okay too, then all producers will be requested to resume.
         */
        public void notifyOkay() {
            BackPressure.this.notifyConsumerOkay(this);
        }

    }

}
