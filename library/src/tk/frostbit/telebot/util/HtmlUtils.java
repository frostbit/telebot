package tk.frostbit.telebot.util;

import org.jetbrains.annotations.NotNull;

import java.util.*;

public class HtmlUtils {

    /**
     * Escapes HTML tags in input string.
     * <p>
     *     Only {@code <}, {@code >} and {@code &} are replaced with corresponding HTML entities.
     * </p>
     */
    public static String escapeTags(@NotNull String input) {
        Objects.requireNonNull(input, "input string must not be null");
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            final char c = input.charAt(i);
            switch (c) {
                case '&':
                    sb.append("&amp;");
                    break;
                case '<':
                    sb.append("&lt;");
                    break;
                case '>':
                    sb.append("&gt;");
                    break;
                default:
                    sb.append(c);
            }
        }
        return sb.toString();
    }

    private HtmlUtils() {
        throw new UnsupportedOperationException();
    }

}
