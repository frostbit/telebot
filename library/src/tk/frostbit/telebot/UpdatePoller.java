package tk.frostbit.telebot;

import tk.frostbit.telebot.model.Update;
import tk.frostbit.telebot.util.BackPressure;

import java.util.*;

public abstract class UpdatePoller implements BackPressure.Producer {

    private final Bot bot;

    public UpdatePoller(Bot bot) {
        this.bot = bot;
    }

    /**
     * Start polling updates.
     */
    protected abstract void start();

    /**
     * Stop polling new updates.
     * <br/> All retrieved updates should be submitted before leaving this method.
     * After that all subsequent updates passed to {@link #submitUpdate(Update)} may be ignored.
     */
    protected abstract void shutdown() throws InterruptedException;

    /**
     * {@inheritDoc}
     *
     * <p>
     * This method is guaranteed to be called between {@link #start()} and {@link #shutdown()}.
     * </p>
     */
    public abstract void pause();

    /**
     * {@inheritDoc}
     *
     * <p>
     * This method is guaranteed to be called between {@link #start()} and {@link #shutdown()}.
     * </p>
     */
    public abstract void resume();

    protected final void submitUpdate(Update update) {
        Objects.requireNonNull(update);
        bot.dispatchUpdate(update);
    }

    public final Bot getBot() {
        return bot;
    }

}
