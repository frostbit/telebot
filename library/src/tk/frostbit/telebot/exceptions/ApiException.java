package tk.frostbit.telebot.exceptions;

public class ApiException extends Exception {

    private final int code;

    public ApiException(String message) {
        this(0, message);
    }

    public ApiException(int code, String message) {
        super(message);
        this.code = code;
    }

    public ApiException(Throwable cause) {
        super(cause);
        this.code = 0;
    }

    public ApiException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
