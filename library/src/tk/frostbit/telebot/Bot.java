package tk.frostbit.telebot;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tk.frostbit.telebot.api.GetMe;
import tk.frostbit.telebot.api.Request;
import tk.frostbit.telebot.exceptions.ApiException;
import tk.frostbit.telebot.impl.DefaultApiAccessor;
import tk.frostbit.telebot.impl.DefaultUpdatePoller;
import tk.frostbit.telebot.model.Chat;
import tk.frostbit.telebot.model.Update;
import tk.frostbit.telebot.model.User;
import tk.frostbit.telebot.util.BackPressure;

import java.util.*;
import java.util.function.Consumer;

public abstract class Bot {

    private static final String API_URL_FORMAT = "https://api.telegram.org/bot%s/%s";
    private static final Logger logger = LoggerFactory.getLogger(Bot.class);

    private final BotConfig config;
    private final BackPressure backPressure;

    private volatile BotState state = BotState.NOT_STARTED;
    private List<CommandHandler> handlers;
    private UpdatePoller poller;
    private ApiAccessor apiAccessor;
    private User self;

    public Bot(BotConfig config) {
        this.config = config.copy();
        this.config.validate();
        this.backPressure = new BackPressure();
    }

    public synchronized void start() throws ApiException {
        if (state == BotState.NOT_STARTED) {
            state = BotState.STARTING;

            final int handlerCount = config.getHandlerThreads();
            final List<CommandHandler> handlers = new ArrayList<>(handlerCount);
            for (int i = 0; i < handlerCount; i++) {
                final CommandHandler handler = createCommandHandler();
                handlers.add(handler);
                handler.start();
            }
            this.handlers = Collections.unmodifiableList(handlers);

            apiAccessor = createApiAccessor();
            apiAccessor.start();

            self = makeRequest(new GetMe());
            if (self == null) {
                throw new ApiException("Failed to get bot's user. Please check API token.");
            }
            logger.debug("Self user: {}", self);

            poller = createUpdatePoller();
            poller.start();
            backPressure.registerProducer(poller);

            state = BotState.WORKING;
        } else {
            throw new IllegalStateException(state.name());
        }
    }

    public synchronized void shutdown() throws InterruptedException {
        if (state == BotState.WORKING) {
            state = BotState.STOPPING;

            backPressure.unregisterProducer(poller);
            poller.shutdown();
            for (CommandHandler handler : handlers) {
                handler.stop();
            }
            apiAccessor.stop();

            state = BotState.STOPPED;
        } else {
            throw new IllegalStateException(state.name());
        }
    }

    @NotNull
    protected abstract CommandHandler createCommandHandler();

    @NotNull
    protected UpdatePoller createUpdatePoller() {
        return new DefaultUpdatePoller(this);
    }

    @NotNull
    protected ApiAccessor createApiAccessor() {
        return new DefaultApiAccessor(this);
    }

    final void dispatchUpdate(Update update) {
        final long sourceId;
        final Chat chat = update.getChat();
        if (chat != null) {
            sourceId = chat.getId();
        } else {
            final User user = update.getFrom();
            sourceId = user.getId();
        }
        final int handlerIndex = (int) (sourceId % handlers.size());
        final CommandHandler handler = handlers.get(handlerIndex);
        handler.submit(update);
    }

    /**
     * Make synchronous request to Telegram API using bot's credentials.
     * <br/>Can be called from any context when the bot is working.
     *
     * @param request    request to be sent
     * @param <R>        type of the result
     * @return result of the request
     */
    public final <R> R makeRequest(Request<R> request) throws ApiException {
        if (state != BotState.WORKING && state != BotState.STARTING) {
            throw new IllegalStateException("Can't send requests in " + state + " state");
        }
        return apiAccessor.makeSyncRequest(request);
    }

    final <R> R dispatchSyncRequest(Request<R> request, CommandHandler handler) {
        try {
            return apiAccessor.makeSyncRequest(request);
        } catch (ApiException e) {
            // TODO: design exception handing
            throw new RuntimeException(e);
        }
    }

    final <R> void dispatchAsyncRequest(Request<R> request, CommandHandler handler,
                                        Consumer<R> resultConsumer) {
        throw new UnsupportedOperationException();
    }

    public final BotState getState() {
        return state;
    }

    public BotConfig getConfig() {
        return config;
    }

    public BackPressure getBackPressure() {
        return backPressure;
    }

    public User getSelf() {
        return self;
    }

    public String getMethodUrl(String method) {
        return String.format(API_URL_FORMAT, config.getToken(), method);
    }

}
