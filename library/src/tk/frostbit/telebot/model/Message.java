package tk.frostbit.telebot.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONObject;

import java.util.*;

public final class Message {

    private final long id;
    private final User from;
    private final Date date;
    @NotNull
    private final Chat chat;
    @Nullable
    private final String text;

    public Message(JSONObject json) {
        this.id = json.getLong("message_id");
        this.from = new User(json.getJSONObject("from"));
        this.date = new Date(json.getLong("date") * 1000);
        this.chat = new Chat(json.getJSONObject("chat"));
        this.text = json.optString("text", null);
        // TODO: process other fields
    }

    public long getId() {
        return id;
    }

    public User getFrom() {
        return from;
    }

    public Date getDate() {
        return date;
    }

    @NotNull
    public Chat getChat() {
        return chat;
    }

    @Nullable
    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder()
                .append("{id=").append(id)
                .append(", from=").append(from)
                .append(", date=").append(date)
                .append(", chat=").append(chat);
        if (text != null) {
            sb.append(", text='").append(text).append('\'');
        }
        return sb.append("}").toString();
    }
}
