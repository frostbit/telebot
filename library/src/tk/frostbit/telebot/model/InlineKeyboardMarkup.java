package tk.frostbit.telebot.model;

import org.json.JSONObject;
import org.json.JSONString;

import java.util.*;

public final class InlineKeyboardMarkup implements JSONString {

    private final List<List<InlineKeyboardButton>> inlineKeyboard;

    public InlineKeyboardMarkup(List<List<InlineKeyboardButton>> markup) {
        this.inlineKeyboard = Objects.requireNonNull(markup, "markup");
    }

    @Override
    public String toJSONString() {
        final JSONObject json = new JSONObject();
        json.put("inline_keyboard", inlineKeyboard);
        return json.toString();
    }

    public static final class Builder {

        private final List<List<InlineKeyboardButton>> buttons = new ArrayList<>();

        public Builder() {
            buttons.add(new ArrayList<>());
        }

        public Builder add(InlineKeyboardButton button) {
            buttons.get(buttons.size() - 1).add(button);
            return this;
        }

        public Builder addUrl(String text, String url) {
            return add(InlineKeyboardButton.forUrl(text, url));
        }

        public Builder addCallback(String text, String callbackData) {
            return add(InlineKeyboardButton.forCallback(text, callbackData));
        }

        public Builder nextRow() {
            buttons.add(new ArrayList<>());
            return this;
        }

        public InlineKeyboardMarkup build() {
            return new InlineKeyboardMarkup(buttons);
        }

    }

}
