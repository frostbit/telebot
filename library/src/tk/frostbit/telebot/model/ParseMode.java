package tk.frostbit.telebot.model;

public enum ParseMode {
    MARKDOWN("Markdown"),
    HTML("HTML"),
    ;

    private final String apiValue;

    ParseMode(String apiValue) {
        this.apiValue = apiValue;
    }

    public String getApiValue() {
        return apiValue;
    }
}
