package tk.frostbit.telebot.model;

public enum ChatType {
    PRIVATE,
    GROUP,
    SUPERGROUP,
    CHANNEL,
    ;
}
