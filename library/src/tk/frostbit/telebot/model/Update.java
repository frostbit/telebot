package tk.frostbit.telebot.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONObject;

import java.util.*;

public final class Update {

    private final long id;
    private final UpdateType type;
    private final Message message;
    private final Message editedMessage;
    private final InlineQuery inlineQuery;
    private final InlineResult inlineResult;
    private final CallbackQuery callbackQuery;

    public Update(JSONObject json) {
        this.id = json.getLong("update_id");

        UpdateType type = null;

        final JSONObject messageJson = json.optJSONObject("message");
        if (messageJson != null) {
            type = UpdateType.MESSAGE;
            message = new Message(messageJson);
        } else {
            message = null;
        }

        final JSONObject inlineQueryJson = json.optJSONObject("inline_query");
        if (inlineQueryJson != null) {
            type = UpdateType.INLINE_QUERY;
            inlineQuery = new InlineQuery(inlineQueryJson);
        } else {
            inlineQuery = null;
        }

        final JSONObject inlineResultJson = json.optJSONObject("chosen_inline_result");
        if (inlineResultJson != null) {
            type = UpdateType.INLINE_RESULT;
            inlineResult = new InlineResult(inlineResultJson);
        } else {
            inlineResult = null;
        }

        final JSONObject callbackQueryJson = json.optJSONObject("callback_query");
        if (callbackQueryJson != null) {
            type = UpdateType.CALLBACK_QUERY;
            callbackQuery = new CallbackQuery(callbackQueryJson);
        } else {
            callbackQuery = null;
        }

        final JSONObject editedMessageJson = json.optJSONObject("edited_message");
        if (editedMessageJson != null) {
            type = UpdateType.EDITED_MESSAGE;
            editedMessage = new Message(editedMessageJson);
        } else {
            editedMessage = null;
        }

        this.type = Objects.requireNonNull(type);
    }

    public long getId() {
        return id;
    }

    public UpdateType getType() {
        return type;
    }

    public Message getMessage() {
        return message;
    }

    public Message getEditedMessage() {
        return editedMessage;
    }

    public InlineQuery getInlineQuery() {
        return inlineQuery;
    }

    public InlineResult getInlineResult() {
        return inlineResult;
    }

    public CallbackQuery getCallbackQuery() {
        return callbackQuery;
    }

    @NotNull
    public User getFrom() {
        switch (type) {
            case MESSAGE:
                return message.getFrom();
            case EDITED_MESSAGE:
                return editedMessage.getFrom();
            case INLINE_QUERY:
                return inlineQuery.getFrom();
            case INLINE_RESULT:
                return inlineResult.getFrom();
            case CALLBACK_QUERY:
                return callbackQuery.getFrom();
            default:
                throw new AssertionError();
        }
    }

    @Nullable
    public Chat getChat() {
        switch (type) {
            case MESSAGE:
                return message.getChat();
            case EDITED_MESSAGE:
                return editedMessage.getChat();
            case INLINE_QUERY:
            case INLINE_RESULT:
                return null;
            case CALLBACK_QUERY:
                final Message message = callbackQuery.getMessage();
                return (message != null) ? message.getChat() : null;
            default:
                return null;
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder()
                .append("{id=").append(id)
                .append(", type=").append(type).append(", ");
        switch (type) {
            case MESSAGE:
                sb.append("message=").append(message);
                break;
            case EDITED_MESSAGE:
                sb.append("edited=").append(editedMessage);
                break;
            case INLINE_QUERY:
                sb.append("query=").append(inlineQuery);
                break;
            case INLINE_RESULT:
                sb.append("result=").append(inlineResult);
                break;
            case CALLBACK_QUERY:
                sb.append("query=").append(callbackQuery);
                break;
        }
        return sb.append("}").toString();
    }
    
}
