package tk.frostbit.telebot.model;

import org.json.JSONObject;

public final class InlineResult {

    private final String id;
    private final User from;
    private final String query;

    public InlineResult(JSONObject json) {
        this.id = json.getString("result_id");
        this.from = new User(json.getJSONObject("from"));
        this.query = json.getString("query");
    }

    public String getId() {
        return id;
    }

    public User getFrom() {
        return from;
    }

    public String getQuery() {
        return query;
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", from=" + from +
                ", query='" + query + '\'' +
                '}';
    }
}
