package tk.frostbit.telebot.model;

import org.json.JSONObject;

public final class Chat {

    private final long id;
    private final ChatType type;

    public Chat(JSONObject json) {
        this.id = json.getLong("id");
        this.type = ChatType.valueOf(json.getString("type").toUpperCase());
    }

    public long getId() {
        return id;
    }

    public ChatType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", type=" + type +
                '}';
    }
}
