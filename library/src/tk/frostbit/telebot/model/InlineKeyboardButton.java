package tk.frostbit.telebot.model;

import org.json.JSONObject;
import org.json.JSONString;

import java.util.*;

public final class InlineKeyboardButton implements JSONString {

    private final String text;
    private String url;
    private String callbackData;

    private InlineKeyboardButton(String text) {
        this.text = Objects.requireNonNull(text, "text");
    }

    public static InlineKeyboardButton forUrl(String text, String url) {
        final InlineKeyboardButton result = new InlineKeyboardButton(text);
        result.url = Objects.requireNonNull(url, "url");
        return result;
    }

    public static InlineKeyboardButton forCallback(String text, String callbackData) {
        final InlineKeyboardButton result = new InlineKeyboardButton(text);
        result.callbackData = Objects.requireNonNull(callbackData, "callbackData");
        return result;
    }

    public String getText() {
        return text;
    }

    public String getUrl() {
        return url;
    }

    public String getCallbackData() {
        return callbackData;
    }

    @Override
    public String toJSONString() {
        final JSONObject json = new JSONObject();
        json.put("text", text);
        json.put("url", url);
        json.put("callback_data", callbackData);
        return json.toString();
    }
}
