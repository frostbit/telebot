package tk.frostbit.telebot.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONObject;

public final class User {

    private final long id;
    @NotNull
    private final String firstName;
    @Nullable
    private final String lastName;
    @Nullable
    private final String userName;

    public User(JSONObject json) {
        this.id = json.getLong("id");
        this.firstName = json.getString("first_name");
        this.lastName = json.optString("last_name", null);
        this.userName = json.optString("username", null);
    }

    public long getId() {
        return id;
    }

    @NotNull
    public String getFirstName() {
        return firstName;
    }

    @Nullable
    public String getLastName() {
        return lastName;
    }

    @Nullable
    public String getUserName() {
        return userName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder()
                .append("{id=").append(id)
                .append(", firstName='").append(firstName).append('\'');
        if (lastName != null) {
            sb.append(", lastName='").append(lastName).append('\'');
        }
        if (userName != null) {
            sb.append(", userName='").append(userName).append('\'');
        }
        return sb.append("}").toString();
    }
}
