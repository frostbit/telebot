package tk.frostbit.telebot.model;

import org.json.JSONObject;

public final class InlineQuery {

    private final String id;
    private final User from;
    private final String query;
    private final String offset;

    public InlineQuery(JSONObject json) {
        this.id = json.getString("id");
        this.from = new User(json.getJSONObject("from"));
        this.query = json.getString("query");
        this.offset = json.getString("offset");
    }

    public String getId() {
        return id;
    }

    public User getFrom() {
        return from;
    }

    public String getQuery() {
        return query;
    }

    public String getOffset() {
        return offset;
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", from=" + from +
                ", query='" + query + '\'' +
                ", offset='" + offset + '\'' +
                '}';
    }
}
