package tk.frostbit.telebot.model;

import org.jetbrains.annotations.Nullable;
import org.json.JSONObject;

public final class CallbackQuery {

    private final String id;
    private final User from;
    @Nullable
    private final Message message;
    @Nullable
    private final String inlineMessageId;
    private final String data;

    public CallbackQuery(JSONObject json) {
        this.id = json.getString("id");
        this.from = new User(json.getJSONObject("from"));
        this.data = json.getString("data");

        final JSONObject messageJson = json.optJSONObject("message");
        this.message = (messageJson != null) ? new Message(messageJson) : null;
        this.inlineMessageId = json.optString("inline_message_id");
    }

    public String getId() {
        return id;
    }

    public User getFrom() {
        return from;
    }

    @Nullable
    public Message getMessage() {
        return message;
    }

    @Nullable
    public String getInlineMessageId() {
        return inlineMessageId;
    }

    public String getData() {
        return data;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder()
                .append("{id='").append(id).append('\'')
                .append(", from=").append(from)
                .append(", data='").append(data).append('\'');
        if (message != null) {
            sb.append(", message=").append(message);
        }
        if (inlineMessageId != null) {
            sb.append(", inlineMessageId='").append(inlineMessageId).append('\'');
        }
        return sb.append("}").toString();
    }
}
