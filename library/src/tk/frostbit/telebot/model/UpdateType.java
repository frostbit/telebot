package tk.frostbit.telebot.model;

public enum UpdateType {
    MESSAGE,
    EDITED_MESSAGE,
    INLINE_QUERY,
    INLINE_RESULT,
    CALLBACK_QUERY,
    ;
}
