package tk.frostbit.telebot;

import tk.frostbit.telebot.api.Request;
import tk.frostbit.telebot.exceptions.ApiException;

public abstract class ApiAccessor {

    private final Bot bot;

    protected ApiAccessor(Bot bot) {
        this.bot = bot;
    }

    protected abstract void start();

    protected abstract void stop() throws InterruptedException;

    protected abstract <R> R makeSyncRequest(Request<R> request) throws ApiException;

    public final Bot getBot() {
        return bot;
    }

}
