package tk.frostbit.telebot;

import java.util.*;

public class BotConfig<C extends BotConfig> implements Cloneable {

    private final Class<? extends C> clazz;

    private String token;
    private int handlerThreads;
    private int handlerQueueLimit;
    private int pollerRetryPeriod;

    public BotConfig(Class<? extends C> clazz) {
        // Set defaults
        handlerThreads = Runtime.getRuntime().availableProcessors();
        handlerQueueLimit = 100;
        pollerRetryPeriod = 60;

        this.clazz = getClass().asSubclass(clazz);
    }

    public String getToken() {
        return token;
    }

    public C setToken(String token) {
        this.token = token;
        return clazz.cast(this);
    }

    public int getHandlerThreads() {
        return handlerThreads;
    }

    public C setHandlerThreads(int handlerThreads) {
        this.handlerThreads = handlerThreads;
        return clazz.cast(this);
    }

    public int getHandlerQueueLimit() {
        return handlerQueueLimit;
    }

    public C setHandlerQueueLimit(int handlerQueueLimit) {
        this.handlerQueueLimit = handlerQueueLimit;
        return clazz.cast(this);
    }

    public int getPollerRetryPeriod() {
        return pollerRetryPeriod;
    }

    public C setPollerRetryPeriod(int pollerRetryPeriod) {
        this.pollerRetryPeriod = pollerRetryPeriod;
        return clazz.cast(this);
    }

    final void validate() {
        Objects.requireNonNull(token, "Token is not set");
        if (handlerThreads < 1) {
            throw new IllegalArgumentException(
                    "Illegal value for handler threads count: " + handlerThreads);
        }
        if (handlerQueueLimit < 1) {
            throw new IllegalArgumentException(
                    "Illegal value for handler queue limit: " + handlerQueueLimit);
        }
    }

    public C copy() {
        try {
            return clazz.cast(super.clone());
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }

}
