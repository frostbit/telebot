package tk.frostbit.telebot;

public enum BotState {
    NOT_STARTED,
    STARTING,
    WORKING,
    STOPPING,
    STOPPED,
    ;
}
