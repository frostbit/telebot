package tk.frostbit.telebot;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tk.frostbit.telebot.api.Request;
import tk.frostbit.telebot.model.*;
import tk.frostbit.telebot.util.BackPressure;

import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class CommandHandler {

    private static final Pattern COMMAND_REGEX = Pattern.compile("^/(\\w{1,32}+)(?:@(\\w++))?(?: (.*))?");
    private static final Logger logger = LoggerFactory.getLogger(CommandHandler.class);

    private final Bot bot;
    private final Thread thread;
    private final BlockingQueue<Runnable> eventQueue;
    private final BackPressure.ConsumerHandle consumer;
    private final int queueLimit;

    private volatile boolean stopped;

    public CommandHandler(Bot bot) {
        Objects.requireNonNull(bot);

        this.bot = bot;
        this.eventQueue = new LinkedBlockingQueue<>();
        this.thread = new Thread(this::eventLoop, "CommandHandler");
        this.consumer = bot.getBackPressure().newConsumer();
        this.queueLimit = bot.getConfig().getHandlerQueueLimit();
    }

    protected void start() {
        if (bot.getState() != BotState.STARTING || thread.getState() != Thread.State.NEW) {
            throw new IllegalStateException();
        }

        thread.start();
    }

    protected void stop() throws InterruptedException {
        stopped = true;
        thread.interrupt();
        thread.join();
    }

    final synchronized void submit(final Update update) {
        eventQueue.add(() -> handleUpdate(update));
        if (eventQueue.size() >= queueLimit) {
            consumer.notifyOverloaded();
        }
    }

    private void eventLoop() {
        while (!stopped || !eventQueue.isEmpty()) {
            final Runnable event;
            try {
                event = eventQueue.take();
            } catch (InterruptedException e) {
                continue;
            }

            event.run();

            if (eventQueue.size() <= queueLimit / 2) {
                consumer.notifyOkay();
            }
        }
    }

    protected void handleUpdate(Update update) {
        switch (update.getType()) {
            case MESSAGE:
                handleMessage(update.getMessage());
                break;
            case EDITED_MESSAGE:
                handleMessageEdited(update.getEditedMessage());
                break;
            case INLINE_QUERY:
                handleInlineQuery(update.getInlineQuery());
                break;
            case INLINE_RESULT:
                onInlineResultChosen(update.getInlineResult());
                break;
            case CALLBACK_QUERY:
                handleCallbackQuery(update.getCallbackQuery());
                break;
            default:
                throw new IllegalArgumentException(update.getType().name());
        }
    }

    protected void handleMessage(Message message) {
        final boolean commandHandled = tryHandleCommand(message);
        if (!commandHandled) {
            handleArbitraryMessage(message);
        }
    }

    private boolean tryHandleCommand(Message message) {
        final String text = message.getText();
        if (text == null) {
            // Not a text message
            return false;
        }

        final Matcher matcher = COMMAND_REGEX.matcher(text);
        if (matcher.find()) {
            final String command = matcher.group(1);
            final String targetName = matcher.group(2);
            final String query = matcher.group(3);
            if (targetName != null) {
                final String selfUserName = getBot().getSelf().getUserName();
                if (!targetName.equalsIgnoreCase(selfUserName)) {
                    // Command is addressed to another bot, ignore message completely
                    return true;
                }
            }
            return handleCommandMessage(command.toLowerCase(), query, message);
        } else {
            return false;
        }
    }

    protected abstract void handleArbitraryMessage(Message message);

    protected boolean handleCommandMessage(@NotNull String command, @Nullable String query, @NotNull Message message) {
        return false;
    }

    protected void handleMessageEdited(Message message) {
        logger.debug("Message edited: " + message);
    }

    protected void handleInlineQuery(InlineQuery query) {
        logger.debug("Inline query: " + query);
    }

    protected void onInlineResultChosen(InlineResult result) {
        logger.debug("Inline result chosen: " + result);
    }

    protected void handleCallbackQuery(CallbackQuery query) {
        logger.debug("Callback query: " + query);
    }

    /**
     * Make request to Telegram API using synchronous mode (in current thread).
     *
     * @param request    the request
     * @param <R>        type of the result
     * @return result of the request
     */
    protected final <R> R makeApiRequest(@NotNull Request<R> request) {
        return bot.dispatchSyncRequest(request, this);
    }

    /**
     * Make asynchronous request to Telegram API.
     *
     * @param request           the request
     * @param resultConsumer    consumer of the request result; will be called inside event loop
     * @param <R>               type of the result
     */
    protected final <R> void makeApiRequestAsync(@NotNull Request<R> request,
                                                 @Nullable Consumer<R> resultConsumer) {
        bot.dispatchAsyncRequest(request, this, result -> {
            if (resultConsumer != null) {
                eventQueue.add(() -> resultConsumer.accept(result));
            }
        });
    }

    public Bot getBot() {
        return bot;
    }
}
