package tk.frostbit.telebot.impl;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tk.frostbit.telebot.Bot;
import tk.frostbit.telebot.UpdatePoller;
import tk.frostbit.telebot.model.Update;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class DefaultUpdatePoller extends UpdatePoller {

    private static final Logger logger = LoggerFactory.getLogger(UpdatePoller.class);
    private static final int LONG_POLLING_TIMEOUT = 20;

    private final CloseableHttpClient httpClient;
    private final Thread thread;
    private final Object sync = new Object();

    private volatile long nextUpdateId;
    private volatile boolean paused;
    private volatile boolean stopped;
    private volatile boolean failed;
    private volatile HttpRequestBase currentRequest;

    public DefaultUpdatePoller(Bot bot) {
        super(bot);
        httpClient = HttpClientBuilder.create()
                .build();
        thread = new Thread(this::loop, "UpdatePoller");
    }

    @Override
    protected void start() {
        thread.start();
    }

    @Override
    protected void shutdown() throws InterruptedException {
        stopped = true;
        final HttpRequestBase request = this.currentRequest;
        if (request != null) {
            request.abort();
        }
        try {
            httpClient.close();
        } catch (IOException e) {
            logger.warn("Exception thrown on closing http client", e);
        }
        thread.interrupt();
        thread.join();
    }

    @Override
    public void pause() {
        paused = true;
    }

    @Override
    public void resume() {
        synchronized (sync) {
            paused = false;
            sync.notify();
        }
    }

    private void loop() {
        final String urlFormat = getBot().getMethodUrl("getUpdates") + "?offset=%d&timeout=%d";
        final int retryPeriod = getBot().getConfig().getPollerRetryPeriod();
        while (!stopped) {
            try {
                synchronized (sync) {
                    while (paused) {
                        sync.wait();
                    }
                }

                try {
                    final String url = String.format(urlFormat, nextUpdateId, LONG_POLLING_TIMEOUT);
                    currentRequest = new HttpGet(url);
                    final HttpResponse response = httpClient.execute(currentRequest);
                    currentRequest = null;

                    final String responseString = EntityUtils.toString(response.getEntity());
                    final JSONObject responseJson = new JSONObject(responseString);
                    if (responseJson.getBoolean("ok")) {
                        final JSONArray updates = responseJson.getJSONArray("result");
                        for (int i = 0; i < updates.length(); i++) {
                            final Update update = new Update(updates.getJSONObject(i));
                            nextUpdateId = Math.max(nextUpdateId, update.getId() + 1);
                            submitUpdate(update);
                        }
                    } else {
                        throw new IOException("Received alert from server: " + responseString);
                    }

                    if (failed) {
                        failed = false;
                        logger.info("Polling recovered");
                    }
                } catch (Exception e) {
                    if (!stopped) {
                        if (!failed) {
                            failed = true;
                            logger.error("Failed to get updates", e);
                            logger.info("Retrying after {} seconds", retryPeriod);
                        }
                        TimeUnit.SECONDS.sleep(retryPeriod);
                    }
                }
            } catch (InterruptedException e) {
                if (!stopped) {
                    logger.warn("Interrupted unexpectedly; ignoring");
                }
            }
        }
    }

}
