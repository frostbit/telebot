package tk.frostbit.telebot.impl;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tk.frostbit.telebot.ApiAccessor;
import tk.frostbit.telebot.Bot;
import tk.frostbit.telebot.api.Request;
import tk.frostbit.telebot.exceptions.ApiException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Stream;

public class DefaultApiAccessor extends ApiAccessor {

    private static final Logger logger = LoggerFactory.getLogger(DefaultApiAccessor.class);

    private final CloseableHttpClient httpClient;

    public DefaultApiAccessor(Bot bot) {
        super(bot);
        httpClient = HttpClientBuilder.create().build();
    }

    @Override
    protected void start() {

    }

    @Override
    protected void stop() throws InterruptedException {
        try {
            httpClient.close();
        } catch (IOException e) {
            logger.warn("Exception thrown on closing http client", e);
        }
    }

    @Override
    protected <R> R makeSyncRequest(Request<R> request) throws ApiException {
        final HttpPost httpPost = new HttpPost(getBot().getMethodUrl(request.getMethod()));
        final Map<String, Object> parameters = request.getParameters();
        final Stream<NameValuePair> pairs = parameters.entrySet().stream()
                .filter(entry -> entry.getValue() != null)
                .map(entry -> new BasicNameValuePair(
                        entry.getKey(), entry.getValue().toString()));
        final UrlEncodedFormEntity entity = new UrlEncodedFormEntity(
                pairs::iterator, StandardCharsets.UTF_8);
        httpPost.setEntity(entity);
        try {
            final CloseableHttpResponse response = httpClient.execute(httpPost);
            final String responseString = EntityUtils.toString(response.getEntity());
            final JSONObject responseJson = new JSONObject(responseString);
            if (responseJson.getBoolean("ok")) {
                return request.parseResult(responseJson.get("result"));
            } else {
                throw new ApiException(responseJson.getInt("error_code"), responseJson.getString("description"));
            }
        } catch (IOException e) {
            throw new ApiException(e);
        }
    }

}
