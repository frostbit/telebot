package tk.frostbit.telebot.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HtmlUtilsTest {

    @Test
    public void escapeTagsSimple() {
        final String input = "<b>Bold text</b>";
        final String expected = "&lt;b&gt;Bold text&lt;/b&gt;";
        final String actual = HtmlUtils.escapeTags(input);

        assertEquals(expected, actual);
    }

    @Test
    public void escapeTagsAmp() {
        final String input = "&foo;";
        final String expected = "&amp;foo;";
        final String actual = HtmlUtils.escapeTags(input);

        assertEquals(expected, actual);
    }

    @Test
    public void escapeTagsEmpty() {
        assertEquals("", HtmlUtils.escapeTags(""));
    }

}
